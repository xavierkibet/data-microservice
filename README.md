Data Tracker
===================
## Description
 - Events tracker is an API that make its possible collect data about the wide range of activities in Moringa school.

------------------------------------------------------------------------

## Getting started
### Requirements
* Python 3.6 +
* Django 2.1.1 +

### Creating a virtual environment
 * You can use virtualenvwrapper / virtualenv or whatever virtual environment tool you use

### Installing dependencies
```bash
pip3 install -r requirements.txt
```

### Prepare environmet variables
* For this project you will need the following configurations.
```bash
DB_NAME='name of db'
DB_USER='db user'
DB_PASSWORD='db pass'
DB_HOST='db host address'
```


### Database migrations

```bash
python manage.py migrate
```

### Running the server 
```bash
python manage.py runserver
```
### Testing the api endpoints
* Use any tool you wish like curl, postman, browser ...

#### Load fixtures
- To test api endpoint using the json below you first need some data of allowed items in the db:
```bash
python manage.py loaddata fixtures/*
```

#### /api-post-new-event
-  The schema for this endpoint looks like this:
```json
    {"events":[
        {   "platform":{
                "name":"app"
            },
            "actor": {
                "uid":"12345",
                "actor_type":"User",
                "name":"john doe"
            },
            "action":{
                "verb":"class.apply"
            },
            "object":{
                "object_type":"Class",
                "uid":"54321",
                "name":"MC9"
            },
            "context":{
                "name":"referral"
            },
            "timestamp":{
                "timestamp":"2018-10-11T16:38:17.844Z",
                "start":"2018-10-11T16:38:17.844Z",
                "stop":"2018-10-11T16:50:17.844Z"
            }
        }
    ]}
```

### Deploying 
- To be deployed to google cloud. Instructions to be added in later iterations.

### Running the tests
```bash
python manage.py test
```

## TODO 

+ [ ] Cache validations lists in a fast storage like redis.
+ [ ] Implement a queue based system
+ [ ] deploy to google cloud using docker containers
+ [ ] authentication with an api key
+ [ ] grpc service - write proto buffs & rpc service for events

# Notes
- Turns the slower speeds observed in iteration2 was as a result of using sqlite a much slower sql based db, we transitioned to postgresql and observed better performance.
- Currently we are trying out grpc, an architecture is therefore needed very soon before moving to grpc iteration.

## Contributing
.......

## License 
.......