from django.test import TestCase
from api.interfaces.EventInterface import Actor


class TestActor(TestCase):
    def setUp(self):
        self.fields = {
            "uid": "test_platform",
            "actor_type": "test_actor",
            "name": "test_action"
        }
        self.test_model = Actor(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model, Actor)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))
