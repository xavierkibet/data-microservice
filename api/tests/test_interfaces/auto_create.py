from api.interfaces.EventInterface import *


def create_action():
    fields = {
        "verb": "class.apply"
    }
    return Action(**fields)


def create_actor():
    fields = {
        "uid": "test_platform",
        "actor_type": "test_actor",
        "name": "test_action"
    }
    return Actor(**fields)


def create_platform():
    fields = {
        "name": "test_action"
    }
    return Platform(**fields)


def create_obj():
    fields = {
        "uid": "test_platform",
        "object_type": "test_Obj",
        "name": "test_action"
    }
    return Obj(**fields)


def create_timestamp():
    fields = {
        "start": "test_platform",
        "stop": "test_Timestamp",
        "timestamp": "test_action"
    }
    return Timestamp(**fields)


def create_context():
    fields = {
        "name": "test_action"
    }
    return Context(**fields)
