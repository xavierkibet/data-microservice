from django.test import TestCase
from api.interfaces.SqlStorageInterface import SQLStorage
from api.configuration import EventInterface


class TestSqlStorage(TestCase):
    def setUp(self):
        self.test_model = SQLStorage(EventInterface)

    def test_initiate(self):
        self.assertIsInstance(self.test_model, SQLStorage)

    def test_store(self):
        pass

    def test_retrieve(self):
        pass
