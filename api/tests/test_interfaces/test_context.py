from django.test import TestCase
from api.interfaces.EventInterface import Context


class TestContext(TestCase):
    def setUp(self):
        self.fields = {
            "name": "test_action"
        }
        self.test_model = Context(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model, Context)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))
