from django.test import TestCase
from api.interfaces.EventInterface import Obj


class TestObj(TestCase):
    def setUp(self):
        self.fields = {
            "uid": "test_platform",
            "object_type": "test_Obj",
            "name": "test_action"
        }
        self.test_model = Obj(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model, Obj)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))
