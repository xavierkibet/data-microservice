from django.test import TestCase
from api.interfaces.EventInterface import Timestamp


class TestTimestamp(TestCase):
    def setUp(self):
        self.fields = {
            "start": "test_platform",
            "stop": "test_Timestamp",
            "timestamp": "test_action"
        }
        self.test_model = Timestamp(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model, Timestamp)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))
