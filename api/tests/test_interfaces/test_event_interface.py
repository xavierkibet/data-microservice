from django.test import TestCase
from .auto_create import create_action, create_actor, create_context, create_obj, create_platform, create_timestamp
from api.interfaces.EventInterface import EventInterface


class TestEventInterface(TestCase):
    def setUp(self):
        self.platform = create_platform()
        self.actor = create_actor()
        self.action = create_action()
        self.obj = create_obj()
        self.timestamp = create_timestamp()
        self.context = create_context()

        self.fields = {
            "platform": self.platform,
            "actor": self.actor,
            "action": self.action,
            "obj": self.obj,
            "timestamp": self.timestamp,
            "context": self.context
        }
        self.test_model = EventInterface(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model, EventInterface)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))
