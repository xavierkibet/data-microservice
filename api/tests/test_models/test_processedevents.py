from django.test import TestCase
from api.dbModels.modelsV1 import ProcessedEvents, Events
from .auto_create import create_events


class TestProcessedevents(TestCase):
    def setUp(self):
        self.fields = {
            "event": create_events(),
            "json": "test_json",
            "created_at": "test_created_at",
            "inserted_at": "test_inserted_at",
        }
        self.test_model = ProcessedEvents(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  ProcessedEvents)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "{} on {}".format(self.test_model.pk, self.fields["created_at"])
        self.assertEquals(expected, str(self.test_model))
