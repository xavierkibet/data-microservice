from django.test import TestCase
from api.dbModels.modelsV1 import UnprocessedEvents


class TestUnprocessedevents(TestCase):
    def setUp(self):
        self.fields = {
            "json": "test_json",
            "created_at": "test_created_at",
            "inserted_at": "test_inserted_at",
        }
        self.test_model = UnprocessedEvents(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  UnprocessedEvents)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "{} on {}".format(self.test_model.pk, self.fields["created_at"])
        self.assertEquals(expected, str(self.test_model))