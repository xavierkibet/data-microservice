from django.test import TestCase
from api.dbModels.modelsV1 import AllowedContexts


class TestAllowedcontexts(TestCase):
    def setUp(self):
        self.fields = {
            "context": "test_context",
        }
        self.test_model = AllowedContexts(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  AllowedContexts)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "Context: {}".format(self.fields["context"])
        self.assertEquals(expected, str(self.test_model))
