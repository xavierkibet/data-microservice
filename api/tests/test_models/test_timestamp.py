from django.test import TestCase
from api.dbModels.modelsV1 import Timestamp


class TestTimestamp(TestCase):
    def setUp(self):
        self.fields = {
            "timestamp": "test_timestamp",
            "start": "test_start",
            "stop": "test_stop",
        }
        self.test_model = Timestamp(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  Timestamp)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "{}".format(self.fields["timestamp"])
        self.assertEquals(expected, str(self.test_model))