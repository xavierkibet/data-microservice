from django.test import TestCase
from api.dbModels.modelsV1 import AllowedActions


class TestAllowedactions(TestCase):
    def setUp(self):
        self.fields = {
            "verb": "test_verb",
        }
        self.test_model = AllowedActions(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  AllowedActions)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "Action: {}".format(self.fields["verb"])
        self.assertEquals(expected, str(self.test_model))