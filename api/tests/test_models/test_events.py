from django.test import TestCase
from .auto_create import create_actor, create_action, create_platform,\
    create_timestamp, create_obj, create_context
from api.dbModels.modelsV1 import Events


class TestEvents(TestCase):
    def setUp(self):
        self.fields = {
            "platform": create_platform(),
            "actor": create_actor(),
            "action": create_action(),
            "obj": create_obj(),
            "context": create_context(),
            "timestamp": create_timestamp(),
            "created_at": "test_created_at",
            "inserted_at": "test_inserted_at",
        }
        self.test_model = Events(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  Events)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "{} {}".format(self.fields["obj"], self.fields["action"])
        self.assertEquals(expected, str(self.test_model))

    def test_create(self):
        # self.new_model = Events.create(**self.fields)
        # self.assertIsInstance(self.new_model, Events)
        pass