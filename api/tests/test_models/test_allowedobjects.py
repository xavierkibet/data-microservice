from django.test import TestCase
from api.dbModels.modelsV1 import AllowedObjects


class TestAllowedobjects(TestCase):
    def setUp(self):
        self.fields = {
            "obj": "test_obj",
        }
        self.test_model = AllowedObjects(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  AllowedObjects)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "Object: {}".format(self.fields["obj"])
        self.assertEquals(expected, str(self.test_model))