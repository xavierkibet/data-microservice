from api.dbModels.modelsV1 import Action, Actor, AllowedActions, AllowedActors, AllowedContexts,\
    AllowedObjects, AllowedPlatforms, Context, Events, Obj, Platform, ProcessedEvents, Timestamp, UnprocessedEvents


def create_action():
    fields = {
        "verb": "test_verb",
    }
    return Action(**fields)


def create_actor():
    fields = {
        "uid": "test_uid",
        "actor_type": "test_actor_type",
        "name": "test_name",
    }
    return Actor(**fields)


def create_allowedactions():
    fields = {
        "verb": "test_verb",
    }
    return AllowedActions(**fields)


def create_allowedactors():
    fields = {
        "actor": "test_actor",
    }
    return AllowedActors(**fields)


def create_allowedcontexts():
    fields = {
        "context": "test_context",
    }
    return AllowedContexts(**fields)


def create_allowedobjects():
    fields = {
        "obj": "test_obj",
    }
    return AllowedObjects(**fields)


def create_allowedplatforms():
    fields = {
        "platform": "test_platform",
    }
    return AllowedPlatforms(**fields)


def create_context():
    fields = {
        "name": "test_name",
    }
    return Context(**fields)


def create_events():
    fields = {
        "platform": create_platform(),
        "actor": create_actor(),
        "action": create_action(),
        "obj": create_obj(),
        "context": create_context(),
        "timestamp": create_timestamp(),
        "created_at": "test_created_at",
        "inserted_at": "test_inserted_at",
    }
    return Events(**fields)


def create_obj():
    fields = {
        "uid": "test_uid",
        "object_type": "test_object_type",
        "name": "test_name",
    }
    return Obj(**fields)


def create_platform():
    fields = {
        "name": "test_name",
    }
    return Platform(**fields)


def create_processedevents():
    fields = {
        "event": create_events(),
        "json": "test_json",
        "created_at": "test_created_at",
        "inserted_at": "test_inserted_at",
    }
    return ProcessedEvents(**fields)


def create_timestamp():
    fields = {
        "timestamp": "test_timestamp",
        "start": "test_start",
        "stop": "test_stop",
    }
    return Timestamp(**fields)


def create_unprocessedevents():
    fields = {
        "json": "test_json",
        "created_at": "test_created_at",
        "inserted_at": "test_inserted_at",
    }
    return UnprocessedEvents(**fields)
