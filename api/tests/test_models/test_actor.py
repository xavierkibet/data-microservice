from django.test import TestCase
from api.dbModels.modelsV1 import Actor


class TestActor(TestCase):
    def setUp(self):
        self.fields = {
            "uid": "test_uid",
            "actor_type": "test_actor_type",
            "name": "test_name",
        }
        self.test_model = Actor(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  Actor)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "{} {}".format(self.fields["uid"], self.fields["actor_type"])
        self.assertEquals(expected, str(self.test_model))