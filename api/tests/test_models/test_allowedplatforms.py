from django.test import TestCase
from api.dbModels.modelsV1 import AllowedPlatforms


class TestAllowedplatforms(TestCase):
    def setUp(self):
        self.fields = {
            "platform": "test_platform",
        }
        self.test_model = AllowedPlatforms(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  AllowedPlatforms)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "Platform: {}".format(self.fields["platform"])
        self.assertEquals(expected, str(self.test_model))
