from django.test import TestCase
from api.dbModels.modelsV1 import AllowedActors


class TestAllowedactors(TestCase):
    def setUp(self):
        self.fields = {
            "actor": "test_actor",
        }
        self.test_model = AllowedActors(**self.fields)

    def test_initiate(self):
        self.assertIsInstance(self.test_model,  AllowedActors)

    def test_attrs(self):
        for key, value in self.fields.items():
            self.assertTrue(hasattr(self.test_model, key))
            self.assertEquals(value, getattr(self.test_model, key))

    def test_str(self):
        expected = "Actor: {}".format(self.fields["actor"])
        self.assertEquals(expected, str(self.test_model))
