from django.test import TestCase
import sys
from api.handlers.ErrorLogHandler import ErrorLogHandler


class TestErrorLogHandler(TestCase):
    def setUp(self):
        self.test_size = 10
        self.test_model = ErrorLogHandler()

    def test_initiate(self):
        self.assertIsInstance(self.test_model, ErrorLogHandler)

    def test_logger(self):

        for error in Exception.__subclasses__()[:self.test_size]:
            try:
                raise error("Test Error")
            except error as e:
                exec_info = sys.exc_info()
                self.test_model.logError(exec_info, e)
                # TODO
                # [1] Assert Stored
