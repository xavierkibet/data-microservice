import json

from api.handlers.TransformHandler import TransformHandler
from django.test import TestCase
from typing import Generator
from api.configuration import EventSerializer


class TestTransformHandler(TestCase):

    def setUp(self):
        self.test_object = TransformHandler(EventSerializer)

    def test_init(self):
        self.assertIsInstance(self.test_object, TransformHandler)
        self.assertIs(EventSerializer, self.test_object.EventSerializer)

    def test_transform(self):
        events = [
            {
                "actor": "Student",
                "action": "class.apply",
                "object": "Class",
                "dateTime": "10/09/2018 00:44:54",
                "context": "Referral"
            },
        ]
        output = self.test_object.transformEvents({"events":events})

        self.assertIsInstance(output, Generator)
        for inputs, result in zip(output, events):
            self.assertIsInstance(result, dict)
            for key, value in inputs.items():
                self.assertEqual(value, result.get(key))
