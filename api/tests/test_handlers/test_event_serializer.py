from django.test import TestCase
from api.handlers.EventSerializer import EventSerializer
from .test_data import good_data


class TestActor(TestCase):
    fixtures = ['api/tests/fixtures.json', ]
    def setUp(self):
        self.test_model = EventSerializer()

    def test_initiate(self):
        self.assertIsInstance(self.test_model, EventSerializer)

    def test_get_allowed_list(self):
        to_be_defined = ['allowedverbs', 'allowobjects', 'allowedplatforms', 'allowedcontexts', 'allowedactors']
        for prop in to_be_defined:
            self.assertFalse(hasattr(self.test_model, prop))
        self.test_model.get_allowed_list()
        to_be_defined = ['allowedverbs', 'allowobjects', 'allowedplatforms', 'allowedcontexts', 'allowedactors']
        for prop in to_be_defined:
            self.assertTrue(type(getattr(self.test_model, prop, False)) is list)

    def test_define_json_schema(self):
        self.test_model.get_allowed_list()
        self.assertFalse(hasattr(self.test_model, 'schema'))
        self.test_model.define_json_schema()
        self.assertTrue(hasattr(self.test_model, 'schema'))
        self.assertIsInstance(self.test_model.schema, dict)

    def test_validate_json(self):
        self.assertTrue(self.test_model.validate_json({}))
