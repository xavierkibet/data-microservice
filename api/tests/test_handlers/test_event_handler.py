import json

from django.test import TestCase, Client
from api.configuration import StorageInterface, TransformHandler, EventInterface, EventSerializer
from api.handlers .EventHandler import EventHandler
from .test_data import good_data


class TestEventHandler(TestCase):
    fixtures = ['api/tests/fixtures.json', ]
    def setUp(self):
        self.test_class = EventHandler()
        self.test_client = Client()
        self.api_url = '/api-post-new-event'

    def test_initiate(self):
        self.assertIsInstance(self.test_class, EventHandler)

    def test_injection(self):
        self.test_class.injectDependencies(
            StorageInterface, TransformHandler,
            EventSerializer, EventInterface
        )
        self.assertIsInstance(self.test_class.storageInterface, StorageInterface)
        self.assertIsInstance(self.test_class.transformer, TransformHandler)
        self.assertEquals(self.test_class.EventSerializer, EventSerializer)

    def test_validate(self):
        bad_data = [
            {},  # empty JSON
            {"events": []},  # empty event list
            {"events": None},  # wrong data type -> None
            {"events": 'strings'}  # Wrong data type -> string
        ]
        expected_good_response = {"Success": "Events logged"}
        for bad in bad_data:
            response = self.test_client.post(self.api_url, bad)
            self.assertIn(response.status_code, [400, 500], bad)
            self.assertJSONNotEqual(response.content, expected_good_response)

        for good in good_data:
            response = self.test_client.post(self.api_url, good)
            # self.assertEquals(response.status_code, 200)
            # self.assertJSONEqual(response.content, expected_good_response)