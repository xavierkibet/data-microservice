import sys
import os
from typing import List,Dict
import jsonschema
from api.handlers.AbstractEventSerializer import AbstractEventSerializer
from api.dbModels.modelsV1 import AllowedActions,AllowedActors,AllowedContexts,AllowedObjects,AllowedPlatforms
from api.handlers.ErrorLogHandler import ErrorLogHandler

class EventSerializer(AbstractEventSerializer):
    def __init__(self,*args, **kwargs):
        self.elogger=ErrorLogHandler()

    def get_allowed_list(self) -> bool:
        try:
            # to load from memcached db to avoid db conns everytime a user posts
            self.allowedverbs:List=[i.verb for i in AllowedActions.objects.all()]
            self.allowobjects:List=[i.obj for i in AllowedObjects.objects.all()]
            self.allowedplatforms:List= [i.platform for i in AllowedPlatforms.objects.all()]
            self.allowedcontexts:List= [i.context for i in AllowedContexts.objects.all()]
            self.allowedactors:List = [i.actor for i in AllowedActors.objects.all()]
            return True

        except Exception as e:
            self.elogger.logError(sys.exc_info(),e,True)
            return False

    def define_json_schema(self)-> bool:
        try:
            self.schema={ "$schema": "http://json-schema.org/schema#",
                "title": "Event Schema",
                "type": "object",
                "properties": {
                    "events": {
                    "type": "array",
                    "items": { "$ref": "#/definitions/event" }
                    }
                },
                "definitions":{
                    "event":{
                            "type":"object",
                            "properties":{
                                "platform": {
                                    "type": "object",
                                    "properties": {
                                        "name": {"enum": self.allowedplatforms}, 
                                    },
                                    "required": ["name"]
                                },
                                "actor": {
                                    "type": "object",
                                    "properties": {
                                        "actor_type": {"enum": self.allowedactors},
                                        "uid": {"type": "string"},
                                        "name":{"type":"string"},
                                    },
                                    "required": ["actor_type", "uid","name"]
                                },
                                "action": {
                                    "type": "object",
                                    "properties": {
                                        "verb": {"enum": self.allowedverbs}, 
                                    },
                                    "required": ["verb"]
                                },
                                "object": {
                                    "type": "object",
                                    "properties": {
                                        "object_type": {"enum": self.allowobjects},
                                        "uid": {"type": "string"},
                                        "name":{"type":"string"},
                                    },
                                    "required": ["object_type","uid","name"]
                                },
                                "context": {
                                    "type": "object",
                                    "properties": {
                                        "name":{"enum": self.allowedcontexts},
                                    },
                                    "required": ["name"]
                                },
                                "timestamp":{
                                    "type":"object",
                                    "properties":{
                                        "timestamp":{"type":"string"},
                                        "start":{"type":"string"},
                                        "stop":{"type":"string"},
                                    },
                                    "required":["timestamp"]
                                    },
                            },
                            "additionalProperties": False,
                            "required": ["platform","actor","action","object","context","timestamp"],
                    }
                },
                }
            return True
        except Exception as e:
            self.elogger.logError(sys.exc_info(),e,True)
            return False
    
    def validate_json(self,json_data:Dict) -> bool:
        try:
            if not self.get_allowed_list():
                return False
            if not self.define_json_schema():return False 
            jsonschema.validate(json_data,self.schema)
            
            return True
        except Exception as e:
            #log the error
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            print(e)
            return False




