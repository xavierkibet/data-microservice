from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.request import Request
import abc

class AbstractEventHandler(APIView):
    __metaclass__ = abc.ABCMeta

    @abc.abstractclassmethod
    def injectDependencies(cls,StorageInterface, TransformHandler, EventSerializer,EventInterface):
        cls.storageInterface=StorageInterface(EventSerializer,EventInterface)
        cls.transformer=TransformHandler(EventSerializer)
        cls.EventSerializer=EventSerializer

    @abc.abstractmethod
    def post(self, request:Request, *args, **kwargs) -> Response:
        pass

    @abc.abstractmethod
    def validateRequest(self,request:Request) -> bool:
        pass
