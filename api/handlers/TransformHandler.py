from typing import Generator,List,Dict
import abc
from api.handlers.AbstractTransformHandler import AbstractTransformationHandler
from api.handlers.ErrorLogHandler import ErrorLogHandler


class TransformHandler(AbstractTransformationHandler):

    def __init__(self, EventSerializer):
        self.EventSerializer = EventSerializer


    def validateJsonResponse(self, json_data:Dict) -> bool:
        eventserializer=self.EventSerializer()
    
        if not eventserializer.validate_json(json_data):
            return False
        return True
    
    def transformEvents(self, json_data:Dict) -> Generator[Dict,None,None]:
        for event in json_data["events"]:
            yield event

