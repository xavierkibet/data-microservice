import sys
import os
from rest_framework.response import Response
from rest_framework.request import Request
from .AbstractEventHandler import AbstractEventHandler
from typing import Generator,Dict
from api.handlers.ErrorLogHandler import ErrorLogHandler



class EventHandler(AbstractEventHandler):
    '''
    Event handler without queue
    '''

    def __init__(self, **kwargs):
        self.elogger=ErrorLogHandler()
        return super().__init__(**kwargs)

    @classmethod
    def injectDependencies(cls,StorageInterface, TransformHandler, EventSerializer,EventInterface):
        cls.storageInterface=StorageInterface(EventInterface)
        cls.transformer=TransformHandler(EventSerializer)
        cls.EventSerializer=EventSerializer
 
    def post(self, request:Request, *args, **kwargs) -> Response:
        if not self.validateRequest(request):
            return Response({"Error":"bad request"}, status=400)
        if not self.transformEventAndStore():
            return Response({"Error":"An error occured while processing event"}, status=500)
        return Response({"Success":"Events logged"}, status=200)
    
    def validateRequest(self, request:Request) -> bool:
        try:
            self.events=request.data
            if len(self.events["events"])<1:  
                return False
            return True
        except Exception as e:
            #log_the_error
            self.elogger.logError(sys.exc_info(),e,True)
            return False
    
    def transformEventAndStore(self) -> bool:
        try:
            if not self.transformer.validateJsonResponse(self.events):return False
            eventsGenerator:Generator[Dict,None,None] = self.transformer.transformEvents(self.events)
            if self.storageInterface.store(eventsGenerator):
                return True
            return False
            
        except Exception as e:
            # log err
            self.elogger.logError(sys.exc_info(),e,True)
            return False

        

