import abc


class AbstractEventSerializer(abc.ABC):
    
    @abc.abstractmethod
    def __init__(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def get_allowed_list(self) -> bool:
        pass

    @abc.abstractmethod
    def define_json_schema(self)-> bool:
        pass

    @abc.abstractmethod
    def validate_json(self,json_data) -> bool:
        pass

    