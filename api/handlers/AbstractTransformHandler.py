from typing import Generator,List,Dict
import abc

class AbstractTransformationHandler(abc.ABC):

    @abc.abstractmethod
    def __init__(self,EventSerializer):
        self.EventSerializer = EventSerializer


    @abc.abstractmethod
    def transformEvents(self,eventDict:List[Dict]) -> Generator[Dict,None,None]:
        pass
