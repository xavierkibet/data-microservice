from django.db import models
from dateutil import parser
# Create your models here.

class AllowedContexts(models.Model):
    context=models.CharField(max_length=255, null=False, blank=False,unique=True)

    def __str__(self):
        return "Context: {}".format(self.context)

    class Meta:
        verbose_name_plural="AllowedContexts"

class AllowedActors(models.Model):
    actor=models.CharField(max_length=255, null=False, blank=False,unique=True)

    def __str__(self):
        return "Actor: {}".format(self.actor)

    class Meta:
        verbose_name_plural="AllowedActors"

class AllowedObjects(models.Model):
    obj=models.CharField(max_length=255, null=False, blank=False,unique=True)

    def __str__(self):
        return "Object: {}".format(self.obj)

    class Meta:
        verbose_name_plural="AllowedObjects"

class AllowedActions(models.Model):
    verb=models.CharField(max_length=255, null=False, blank=False,unique=True)

    def __str__(self):
        return "Action: {}".format(self.verb)

    class Meta:
        verbose_name_plural="AllowedActions"

class AllowedPlatforms(models.Model):
    platform=models.CharField(max_length=255, null=False, blank=False,unique=True)

    def __str__(self):
        return "Platform: {}".format(self.platform)

    class Meta:
        verbose_name_plural="AllowedPlatforms"


class Platform(models.Model):
    name=models.CharField(max_length=255, null=False, blank=False)


    def create(self,*args, **kwargs):
        return super(Platform,self).create(*args, **kwargs)

    def __str__(self):
        return "{}".format(self.name)

class Actor(models.Model):
    uid=models.CharField(max_length=255, null=False, blank=False)
    actor_type=models.CharField(max_length=255, null=False, blank=False)
    name=models.CharField(max_length=255, null=True, blank=True)

    def create(self,*args, **kwargs):
        return super(Actor,self).create(*args, **kwargs)

    def __str__(self):
        return "{} {}".format(self.uid,self.actor_type)

class Action(models.Model):
    verb=models.CharField(max_length=255, null=False, blank=False)

    def create(self,*args, **kwargs):
        return super(Action,self).create(*args, **kwargs)

    def __str__(self):
        return "{}".format(self.verb)

class Obj(models.Model):
    uid=models.CharField(max_length=255, null=False, blank=False)
    object_type=models.CharField(max_length=255, null=False, blank=False)
    name=models.CharField(max_length=255, null=True, blank=True)

    def create(self,*args, **kwargs):
        return super(Obj,self).create(*args, **kwargs)

    def __str__(self):
        return "{} {}".format(self.uid,self.object_type)

class Context(models.Model):
    name=models.CharField(max_length=255, null=True, blank=True)

    def create(self,*args, **kwargs):
        return super(Context,self).create(*args, **kwargs)

    def __str__(self):
        try:
            return "{}".format(self.name)
        except:
            return "{}".format(self.pk)

class Timestamp(models.Model):
    timestamp=models.DateTimeField(null=False)
    start=models.DateTimeField(null=True)
    stop=models.DateTimeField(null=True)

    def create(self,*args, **kwargs):
        kwargs["timestamp"]=parser.parse(kwargs.pop("timestamp"))
        kwargs["start"]=parser.parse(kwargs.pop("start"))
        kwargs["stop"]=parser.parse(kwargs.pop("stop"))
        return super(Timestamp,self).create(*args, **kwargs)

    def __str__(self):
        return "{}".format(self.timestamp)

class Events(models.Model):
    platform=models.ForeignKey('Platform', related_name='platforms', on_delete=models.PROTECT,blank=False)
    actor=models.ForeignKey('Actor', related_name='actors', on_delete=models.PROTECT,blank=False)
    action=models.ForeignKey('Action', related_name='actions', on_delete=models.PROTECT,blank=False)
    obj=models.ForeignKey('Obj', related_name='objs', on_delete=models.PROTECT,blank=False)
    context=models.ForeignKey('Context', related_name='contexts', on_delete=models.PROTECT,blank=False)
    timestamp = models.ForeignKey('Timestamp', related_name='timestamps', on_delete=models.PROTECT,blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    inserted_at = models.DateTimeField(auto_now=True)
 
    def create(self,*args, **kwargs):
        return super(Events,self).create(*args, **kwargs)

    def __str__(self):
        return "{} {}".format(self.obj, self.action)

    class Meta:
        verbose_name_plural="Events"

class ProcessedEvents(models.Model):
    event=models.OneToOneField("Events", on_delete=models.PROTECT, null=True)
    json=models.TextField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    inserted_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} on {}".format(self.pk,self.created_at)
    class Meta:
        verbose_name_plural="ProcessedEvents"

class UnprocessedEvents(models.Model):
    json=models.TextField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    inserted_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return "{} on {}".format(self.pk,self.created_at)
    class Meta:
        verbose_name_plural="UnprocessedEvents"
