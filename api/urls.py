from django.conf.urls import url
from . import views
from api.configuration import StorageInterface, TransformHandler, EventHandler, EventSerializer, EventInterface


EventHandler.injectDependencies(StorageInterface, TransformHandler, EventSerializer, EventInterface)

urlpatterns = [
    url(r'^api-post-new-event$', EventHandler.as_view(), name='api-post-new-event'),
]