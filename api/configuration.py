

# DI configurations
from api.handlers.ErrorLogHandler import ErrorLogHandler
from api.handlers.TransformHandler import TransformHandler
from api.handlers.EventSerializer import EventSerializer
from api.handlers.EventHandler import EventHandler 

from api.interfaces.SqlStorageInterface import SQLStorage
from api.interfaces.EventInterface import EventInterface


StorageInterface = SQLStorage
TransformHandler = TransformHandler
EventSerializer = EventSerializer
EventHandler = EventHandler
EventInterface = EventInterface
ErrorLogHandler=ErrorLogHandler

# valid actions
VALID_ACTIONS=[
                'class.apply', 
                'payment.pay',
                'class.movement',
                'student.grade', 
                'student.note', 
                'class.attend', 
                'content.video.play', 
                'content.video.start', 
                'content.video.end',
                'content.video.pause', 
                'content.text.start', 
                'context.text.end',
                'payment.creation', 
                'job.placement', 
                'job.movement'
            ]