import abc
from typing import Generator,Dict

class AbstractStorageInterface(abc.ABC):
    '''
    abstract storage interface
    '''
    
    def __init__(self,EventInterface):
        self.EventCollection = List[EventInterface]
        pass

    @abc.abstractmethod
    def store(self, events:Generator[Dict, None, None]) -> bool:
        pass

    @abc.abstractmethod
    def retrieve(self) -> 'self.EventCollection':
        pass