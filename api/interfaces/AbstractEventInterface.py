import abc

class AbstractAction(abc.ABC):
    @abc.abstractmethod
    def __init__(self,*args, **kwargs):
        pass

class AbstractActor(abc.ABC):
    @abc.abstractmethod
    def __init__(self,*args, **kwargs):
        pass

class AbstractPlatform(abc.ABC):
    @abc.abstractmethod
    def __init__(self,*args, **kwargs):
        pass

class AbstractContext(abc.ABC):
    @abc.abstractmethod
    def __init__(self,name:str):
        pass

class AbstractObject(abc.ABC):
    @abc.abstractmethod
    def __init__(self,*args, **kwargs):
        pass

class AbstractTimestamp(abc.ABC):
    @abc.abstractmethod
    def __init__(self,*args, **kwargs):
        pass

class AbstractEventInterface(abc.ABC):
    @abc.abstractmethod
    def __init__(self,*args, **kwargs):
        pass
