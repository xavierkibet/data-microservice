import sys
import os
import json
from .AbstractStorageInterface import AbstractStorageInterface
from typing import Generator, List,Dict
from api.dbModels.modelsV1  import Actor,Action,Platform,Context,\
                                    Events,Timestamp,Obj,ProcessedEvents,UnprocessedEvents
from api.handlers.ErrorLogHandler import ErrorLogHandler

class SQLStorage(AbstractStorageInterface):
    '''
    sql based storage
    '''
    def __init__(self, EventInterface):
        self.EventCollection = List[EventInterface]
        self.elogger=ErrorLogHandler()

    def store(self, events: Generator[Dict, None, None]) -> bool:
        event=next(events)
        while events:
            j_string=json.dumps(event)
            try:
                e_kwargs={
                    "actor":Actor.objects.create(**event["actor"]),
                    "action":Action.objects.create(**event["action"]),
                    "platform":Platform.objects.create(**event["platform"]),
                    "context":Context.objects.create(**event["platform"]),
                    "obj":Obj.objects.create(**event["object"]),
                    "timestamp":Timestamp.objects.create(**event["timestamp"]),
                }

                sE=Events.objects.create(**e_kwargs)
                ProcessedEvents.objects.create(event=sE,json=j_string)
                event=next(events)
            except StopIteration as e:
                break
            except Exception as e:
                self.elogger.logError(sys.exc_info(),e,True)
                UnprocessedEvents.objects.create(json=j_string)
                event=next(events)
        return True
    
    def retrieve(self) -> 'self.EventCollection':
        pass