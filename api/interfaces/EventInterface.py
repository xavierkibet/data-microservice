from .AbstractEventInterface import AbstractEventInterface,\
                                AbstractAction,AbstractActor,\
                                AbstractContext,AbstractObject,\
                                AbstractTimestamp,AbstractPlatform

class Action(AbstractAction):
    def __init__(self,*args, **kwargs):
        self.verb:str=kwargs.pop("verb")

class Actor(AbstractActor):
    def __init__(self,*args, **kwargs):
        self.uid:str=kwargs.pop("uid")
        self.actor_type:str=kwargs.pop("actor_type")
        self.name:str=kwargs.pop("name")

class Platform(AbstractPlatform):
    def __init__(self,*args, **kwargs):
        self.name:str=kwargs.pop("name")

class Context(AbstractContext):
    def __init__(self,*args, **kwargs):
        self.name:str=kwargs.pop("name")

class Obj(AbstractObject):
    def __init__(self,*args, **kwargs):
        self.uid:str=kwargs.pop("uid")
        self.object_type:str=kwargs.pop("object_type")
        self.name:str=kwargs.pop("name")

class Timestamp(AbstractTimestamp):
    def __init__(self,*args, **kwargs):
        self.timestamp:str=kwargs.pop("timestamp")
        self.start:str=kwargs.pop("start")
        self.stop:str=kwargs.pop("stop")


class EventInterface(AbstractEventInterface):
    '''
    Event interface
    '''
    def __init__(self,*args, **kwargs):
        self.platform:Platform=kwargs.pop("platform")
        self.actor:Actor=kwargs.pop("actor")
        self.action:Action=kwargs.pop("action")
        self.obj:Obj=kwargs.pop("obj")
        self.timestamp:Timestamp=kwargs.pop("timestamp")
        self.context:Context=kwargs.pop("context")
